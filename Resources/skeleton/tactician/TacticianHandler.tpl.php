<?= "<?php\n"; ?>
<?php /** @var $argument \App\Aplication\Maker\ValueObject\ServiceVO */ ?>

declare(strict_types=1);

namespace <?= $namespace; ?>;

use App\Aplication\Command\<?= $model_for_handler; ?>;
<?php //example output: use Symfony\Component\Console\Command\Command;
if ($arguments) {
    foreach ($arguments as $argument) {
        echo 'use ' . $argument->getClassPath() . ";\n";
    }
}
?>

class <?= $class_name . "\n"; ?>
{
<?php //example output: private $command;
if ($arguments) {
    foreach ($arguments as $argument) {
        echo "    private " . $argument->getParameterName() . ";\n";
    }
    echo "\n";
}
?>
    public function __construct(<?php //example output: CommandBus $command
$i = 0;
if ($arguments) {
    foreach ($arguments as $argument) {
        if ($i > 0) echo ", ";
        echo $argument->getClassName() . " " . $argument->getParameterName();
        $i++;
    }
}
?>)
    {
<?php //example output:  $this->command = $command;
if ($arguments) {
    foreach ($arguments as $argument) {
        echo '        $this->' . str_replace('$', '', $argument->getParameterName()) . ' = ' . $argument->getParameterName() . ";\n";
    }
}
?>
    }

    public function handler(<?= $model_for_handler; ?> $command)
    {
        //$data = $command->...
    }
}
