<?php

declare(strict_types=1);

namespace Brair\Doer\Validator;

use Brair\Doer\Configuration;

class ServiceValidator
{
    public function validServices($services)
    {
        $this->searchServices($services);
    }

    private function searchServices(array $services)
    {
        foreach ($services as $service) {
            if (!array_key_exists($service, Configuration::getConfigServices())) {
                throw new \Exception(sprintf("[ERROR] Cant find service %s !\n", $service));
            }
        }
    }
}
