<?php

declare(strict_types=1);

namespace Brair\Doer\ValueObject;

class ServiceVO
{
    private $argumentName;
    private $classPath;
    private $className;
    private $parameterName;

    public function __construct(
        string $argumentName,
        string $classPath,
        string $className,
        string $parameterName)
    {
        $this->argumentName = $argumentName;
        $this->classPath = $classPath;
        $this->className = $className;
        $this->parameterName = $parameterName;
    }

    public function getArgumentName(): string
    {
        return $this->argumentName;
    }

    public function getClassPath(): string
    {
        return $this->classPath;
    }

    public function getClassName(): string
    {
        return $this->className;
    }

    public function getParameterName(): string
    {
        return $this->parameterName;
    }

    public static function fromArray(array $data): ServiceVO
    {
        return new self(
            $data['service'],
            $data['namespace'],
            $data['class'],
            $data['parameter']);
    }
}
