<?php

declare(strict_types=1);

namespace Brair\Doer\ValueObject;

class ClassFileVO
{
    private $namespace;
    private $suffix;
    private $templateFile;
    private $options;
    private $className;

    public function __construct(
        string $className,
        string $namespace,
        string $suffix,
        string $templateFile,
        ?array $options)
    {
        $this->namespace = $namespace;
        $this->suffix = $suffix;
        $this->templateFile = $templateFile;
        $this->options = $options;
        $this->className = $className;
    }

    public function getNamespace(): string
    {
        return $this->namespace;
    }

    public function getSuffix(): string
    {
        return $this->suffix;
    }

    public function getTemplateFile(): string
    {
        return $this->templateFile;
    }

    public function getOptions(): ?array
    {
        return $this->options;
    }

    public function getClassName(): string
    {
        return $this->className;
    }

    public static function fromArray(string $className, array $data, ?array $options = null)
    {
        return new self($className, $data['namespace'], $data['suffix'], $data['template'], $options);
    }
}
