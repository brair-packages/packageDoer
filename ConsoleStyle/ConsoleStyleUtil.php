<?php

namespace Brair\Doer\ConsoleStyle;

final class ConsoleStyleUtil
{
    public $yaml;
    public $text;

    public function __construct()
    {
        $this->yaml = new ConsoleYamlServiceUtil();
        $this->text = new ConsoleTextManipulatorUtil();
    }
}
