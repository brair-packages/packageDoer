<?php

declare(strict_types=1);

namespace Brair\Doer\ConsoleStyle;

class ConsoleYamlServiceUtil
{
    public function addArgumentsToService(array &$setOfText, ?array $arguments)
    {
        if ($arguments) {
            $setOfText[] = sprintf('    arguments:');
            foreach ($arguments as $argument) {
                $setOfText[] = sprintf('        - "%s"', $argument->getArgumentName());
            }
        }
    }
}
