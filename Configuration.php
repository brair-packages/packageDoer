<?php

declare(strict_types=1);

namespace Brair\Doer;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Yaml\Yaml;

class Configuration // implements ConfigurationInterface
{
    protected static $packageName = 'doer';
    protected static $configPath = 'config/doer.yaml';

    public static function getConfig(): array
    {
        return Yaml::parseFile(self::$configPath)[self::$packageName];
    }

    public static function getConfigServices(): array
    {
        return self::getConfig()['config_services'];
    }

    public static function getConfigSkeleton(): array
    {
        return self::getConfig()['skeleton'];
    }

    public static function getConfigTemplates(?string $templateName = null): array
    {
        if (null === $templateName) {
            return self::getConfig()['template'];
        }

        $config = self::getConfig()['template'][$templateName];
        if (!$config) {
            throw new \Exception(sprintf("Can't find config for %s. Check section `template` in config file [%s]", $templateName, self::$configPath));
        }

        return $config;
    }

}
