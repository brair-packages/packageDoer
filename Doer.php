<?php

declare(strict_types=1);

namespace Brair\Doer;

use Brair\Doer\ConsoleStyle\ConsoleStyleUtil;
use Brair\Doer\Validator\ServiceValidator;
use Brair\Doer\ValueObject\ClassFileVO;
use Brair\Doer\ValueObject\ServiceVO;
use Symfony\Bundle\MakerBundle\ConsoleStyle;
use Symfony\Bundle\MakerBundle\DependencyBuilder;
use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\MakerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;

abstract class Doer implements MakerInterface
{
    protected static $defaultName;

    protected $validator;
    protected $consoleStyleUtil;

    public function __construct()
    {
        $this->validator = new ServiceValidator();
        $this->consoleStyleUtil = new ConsoleStyleUtil();
    }

    protected function createClass(Generator $generator, ClassFileVO $fileTemplate)
    {
        $handlerClassNameDetails = $generator->createClassNameDetails(
            $fileTemplate->getClassName(),
            $fileTemplate->getNamespace(),
            $fileTemplate->getSuffix()
        );

        $generator->generateClass(
            $handlerClassNameDetails->getFullName(),
            $this->getSkeletonDir() . $fileTemplate->getTemplateFile(),
            $fileTemplate->getOptions()
        );
        $generator->writeChanges();
    }

    public static function getCommandName(): string
    {
        if (!static::$defaultName) {
            throw new \Exception('Command name is null!');
        }
        return static::$defaultName;
    }

    public function getSkeletonDir()
    {
        return Configuration::getConfigSkeleton()['path'];
    }

    /**
     * @param array $arguments      || example: ['em', 'logger', 'logger.myChannel', ...]
     * @return array                || config for service from Config::SERVICES
     */
    public function getServices(array $arguments): array
    {
        $this->validator->validServices($arguments);
        $services = Configuration::getConfigServices();

        foreach ($arguments as $key) {
            $argumentsSet[$key] = ServiceVO::fromArray($services[$key]);
        }

        return $argumentsSet ?? [];
    }

    public function interact(InputInterface $input, ConsoleStyle $io, Command $command)
    {
    }

    protected function addDependencies(array $dependencies, string $message = null): string
    {
        $dependencyBuilder = new DependencyBuilder();

        foreach ($dependencies as $class => $name) {
            $dependencyBuilder->addClassDependency($class, $name);
        }

        return $dependencyBuilder->getMissingPackagesMessage(
            $this->getCommandName(),
            $message
        );
    }

    public function configureDependencies(DependencyBuilder $dependencies)
    {
    }
}

